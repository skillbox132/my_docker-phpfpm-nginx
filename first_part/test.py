def test_os_release(host):
    assert host.file("/etc/os-release").contains("Debian")


def test_nginx_is_installed(host):
    assert host.package("nginx").is_installed
    assert host.package("nginx").version.startswith("1.16")


def test_nginx_running_and_enabled(host):
    assert host.service("nginx").is_running
    assert host.service("nginx").is_enabled
